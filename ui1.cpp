#include "ui1.h"
#include <QPainter>
#include <QImage> 
#include <iostream>
#include <fstream>
#include <qlabel.h>
#include <qwidget.h>
#include <stdio.h>
#include <iomanip>
#include <qmouseeventtransition.h>
#include <QMouseEvent> 
#include <opencv2\highgui.hpp>
#include <opencv\cv.hpp>
#include <opencv\cv.h>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\highgui.hpp>
#include <opencv.hpp>

using namespace std;
using namespace cv;

ui1::ui1(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	qr = NULL;
	connect(ui.pushButton,SIGNAL(clicked()),this,SLOT(generate()));
	connect(ui.pushButton_2, SIGNAL(clicked()), this, SLOT(reset()));
	//cout << "bb" << endl;
	//QLabel *l1;
		
}

ui1::~ui1()
{
	if (qr != NULL)
	{
		QRcode_free(qr);
	}
}
void ui1::save()
{
	/*QImage image(340, 340, QImage::Format_Mono);
	//QImage image("C:\\Users\\lijian\\Desktop\\1.jpg");
	QPainter painter(&image);
	QColor background(Qt::white);
	painter.setBrush(background);
	painter.setPen(Qt::NoPen);
	painter.drawRect(400, 0, 340, 340);
	//painter.drawRect(400, 0, 340, 340);
	if (qr != NULL)
	{
		draw(painter, 340, 340);
	}
	image.save("D:\\a.png");*/
}

void ui1::generate()
{
	
	QString str2 = ui.textEdit->toPlainText();
	QString str = ui.textEdit->toPlainText();
	if (qr != NULL)
	{
		QRcode_free(qr);
	}
	qr = QRcode_encodeString(str.toStdString().c_str(),
		6,
		QR_ECLEVEL_H,
		QR_MODE_8,
		1);
	

	
	/*QString str = ui1::ui.textEdit->toPlainText();
	if (qr != NULL)
	{
		QRcode_free(qr);
	}
	qr = QRcode_encodeString(str.toStdString().c_str(),
		6,
		QR_ECLEVEL_H,
		QR_MODE_8,
		1);
	int len = strlen((char*)qr->data);
	int x = sqrt(len);
	ofstream out("out.txt");//output the generated string, representing a qr-code
	out << x << endl;
	for (int i = 0; i < x; i++) {
		for (int j = 0; j < x; j++) {
			out << setw(4) << (int)qr->data[i * x + j];
		}
		out << endl<<endl<<endl;
	}
	update();*/
}
void ui1::draw2(QPainter &painter, int width, int height) {
	img = imread("C://Users//lijian//Desktop//desktop_file//picture//images//1.jpg");
	int qr_width = qr->width;
	//Upper left corner finding pattern
	for (int y = 0; y <= 7; y++)
	{
		for (int x = 0; x <= 7; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) && (l>30))
			{
				float a = 30 / l;
				adjust2(y, x, a);
			}
			else if ((!(b & 0x01)) && (l<205))
			{
				float a = 205 / l;
				adjust2(y, x, a);
			}
		}
	}
	//Upper right corner
	for (int y = 0; y <= 7; y++)
	{
		for (int x = qr_width - 8; x <= qr_width - 1; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) && (l>30))
			{
				float a = 30 / l;
				adjust2(y, x, a);
			}
			else if ((!(b & 0x01)) && (l<205))
			{
				float a = 205 / l;
				adjust2(y, x, a);
			}
		}
	}
	//Bottom-left corner
	for (int y = qr_width - 8; y <= qr_width - 1; y++)
	{
		for (int x = 0; x <= 7; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01))
			{
				float a = 30 / l;
				adjust2(y, x, a);
			}
			else if ((!(b & 0x01)))
			{
				float a = 205 / l;
				adjust2(y, x, a);
			}
		}
	}
	QImage image = Mat2QImage(img);
	QRect target(450, 20, 410, 410);
	painter.drawImage(target, image);
	int scale_x = 10;
	int scale_y = 10;
	//middle
	for (int y = 8; y <= qr_width - 1; y++)
	{
		for (int x = 8; x <= qr_width - 1; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) )
			{
				//float a = 30 / l;
				//adjust1(y, x, a);
				float a = 30.0f - l;
				if (a > 1e-5) {
					continue;
				}
				else {
					drawblack(painter, scale_x, scale_y, x, y);
				}
			}
			else if ((!(b & 0x01)) && (b != 254))
			{
				//float a = 205 / l;
				float a = 205.0f - l;
				if (a > 1e-5) {
					drawwhite(painter, scale_x, scale_y, x, y);
				}
				else {
					continue;
				}
				//adjust1(y, x, a);
			}
		}
	}
	//above timing pattern 
	for (int y = 0; y <= 7; y++)
	{
		for (int x = 8; x <= qr_width - 9; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01))
			{
				//float a = 30 / l;
				float a = 60.0f - l;
				//adjust1(y, x, a);
				if (a  > 1e-5) {
					continue;
				}
				else {
					drawblack(painter, scale_x, scale_y, x, y);
				}
			}
			else if ((!(b & 0x01)) && (b != 254))
			{
				//float a = 205 / l;
				float a = 195.0f - l;
				//adjust1(y, x, a);
				if (a > 1e-5) {
					drawwhite(painter, scale_x, scale_y, x, y);
				}
				else {
					continue;
				}
			}
		}
	}
	//timing pattern left
	for (int y = 8; y <= qr_width - 9; y++)
	{
		for (int x = 0; x <= 7; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01))
			{
				//float a = 30 / l;
				float a = 60.0f - l;
				if (a > 1e-5) {
					continue;
				}
				else {
					drawblack(painter, scale_x, scale_y, x, y);
				}
			}
			else if ((!(b & 0x01)) && (b != 254))
			{
				//float a = 205 / l;
				float a = 195.0f - l;
				if (a > 1e-5) {
					drawwhite(painter, scale_x, scale_y, x, y);
				}
				else {
					continue;
				}
			}
		}
	}
	
	//middle
	/*for (int y = 8; y <= qr_width - 1; y++)
	{
		for (int x = 8; x <= qr_width - 1; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) && (l>30))
			{
				float a = 30 / l;
				adjust1(y, x, a);
			}
			else if ((!(b & 0x01)) && (l<205) && (b != 254))
			{
				float a = 205 / l;
				adjust1(y, x, a);
			}
		}
	}*/
	//above timing pattern 
	/*for (int y = 0; y <= 7; y++)
	{
		for (int x = 8; x <= qr_width - 9; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) && (l>30))
			{
				float a = 30 / l;
				adjust1(y, x, a);
			}
			else if ((!(b & 0x01)) && (l<205) && (b != 254))
			{
				float a = 205 / l;
				adjust1(y, x, a);
			}
		}
	}
	//timing pattern left
	for (int y = 8; y <= qr_width - 9; y++)
	{
		for (int x = 0; x <= 7; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			float l = average(y, x);
			if ((b & 0x01) && (l>30))
			{
				float a = 30 / l;
				adjust1(y, x, a);
			}
			else if ((!(b & 0x01)) && (l<205) && (b != 254))
			{
				float a = 205 / l;
				adjust1(y, x, a);
			}
		}
	}
	*/
	
	//QPainter painter(&image);
	//QRect r(430, 20, 100,100);
	//draw1(painter, 410, 410);
	//ui.label2->setPixmap(QPixmap::fromImage(image));
}
//draw black dot
void ui1::drawblack(QPainter &painter, int scale_x, int scale_y,int x,int y) {
	QRect r1(450 + x * scale_x + 2, 20 + y * scale_y + 2, 6, 6);
	QRadialGradient radialGradient(450 + x * scale_x + scale_x / 2, 20 + y * scale_y + scale_y / 2, 6, 450 + x * scale_x + scale_x / 2, 20 + y * scale_y + scale_y / 2);
	//(200, 100), radius 100, and focus (200, 100), respectively, of the circular gradients,
	//Where the focus and the center coincide, thus forming from the center of the outward gradient effect
	radialGradient.setColorAt(0, QColor(0, 0, 0, 255));
	radialGradient.setColorAt(1, QColor(0, 0, 0, 0));
	radialGradient.setColorAt(1, QColor(0, 0,0, 0));
	painter.setBrush(radialGradient);
	//Draw the circle so that it coincides with the circle above the circular gradient
	painter.drawEllipse(r1);
}
//draw white dot
void ui1::drawwhite(QPainter &painter, int scale_x, int scale_y, int x, int y) { 
	QRect r1(450 + x * scale_x + 2, 20 + y * scale_y + 2, 6, 6);
	QRadialGradient radialGradient(450 + x * scale_x + scale_x / 2, 20 + y * scale_y + scale_y / 2, 6, 450 + x * scale_x + scale_x / 2, 20 + y * scale_y + scale_y / 2);
	radialGradient.setColorAt(0, QColor(255, 255, 255, 255));
	radialGradient.setColorAt(1, QColor(255, 255, 255, 0));
	radialGradient.setColorAt(1, QColor(255, 255, 255, 0));
	painter.setBrush(radialGradient);
	painter.drawEllipse(r1);
	//  painter.drawEllipse(r1);
}
void ui1::draw(QPainter &painter, int width, int height)
{
	//QColor foreground(Qt::black);
	//painter.setBrush(foreground);
	const int qr_width = qr->width;
	double scale_x = width / qr_width;
	double scale_y = height / qr_width;
	QImage image("C:\\Users\\lijian\\Desktop\\picture\\1.jpg");
	QRect target(450, 20, 410, 410); //Create rect to show the image
									//QPainter painter(this);
	painter.drawImage(target, image);
	ofstream ou("out1.txt");
	ou << qr_width << endl;
	for (int y = 0; y < qr_width; y++)
	{
		for (int x = 0; x < qr_width; x++)
		{
			unsigned char b = qr->data[y * qr_width + x];
			if (b == 254) continue;
			if (b & 1)
			{
				QRect r(450 + x * scale_x, 20 + y * scale_y, scale_x, scale_y);
				///if (b <= 3) {
				QColor background(Qt::black);
				painter.setBrush(background);
			    painter.drawRects(&r,1);
				//}
				//else {
				//	QColor background(Qt::black);
					//painter.setBrush(background);
					//painter.drawRects(&r, 1);
				//}				
			}
			else {
				  QRect r(450 + x * scale_x, 20 + y * scale_y, scale_x, scale_y);
				  QColor background(Qt::white);
				  painter.setBrush(background);
				  painter.drawRects(&r, 1);
	
			}
		}
	}
}
bool ui1::Judge(QPoint *p) {
	int x1 = 10, y1 = 10;
	int x2 = 420, y2 = 420;
	if (p->x() < x1 || p->x() >= x2) {
		return false;
	}
	if (p->y() < y1 || p->y() >= y2) {
		return false;
	}
	return true;
}
void ui1::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QImage image("C://Users//lijian//Desktop//desktop_file//picture//images//1.jpg");
	QRect target(10, 20, 410, 410); //Create a rect to show the image
	//QPainter painter(this);	
	QColor background(Qt::white);
	painter.setBrush(background);
	painter.setPen(Qt::NoPen);
	//painter.drawImage(image);
	painter.drawRect(440, 10, 430, 430);
	painter.drawImage(target, image);
	if (qr != NULL)
	{
		draw2(painter, 410, 410);
	}
	QPen pen;                                 //create a pen  
	pen.setColor(Qt::darkCyan);
	pen.setWidth(5);
	painter.setPen(pen);
	ofstream outf("point.txt");
	for (int i = 0; i<lines.size(); i++) {
		myLine* pLine = lines[i];
		if (Judge(&pLine->startPnt) && Judge(&pLine->endPnt)) {
			outf << pLine->startPnt.x() << " " << pLine->startPnt.y() << endl;
			painter.drawLine(pLine->startPnt, pLine->endPnt);
		}			
	}
	//lines.clear();
	update();
}

void ui1::mousePressEvent(QMouseEvent *e) {
	setCursor(Qt::PointingHandCursor);
	startPnt = e->pos();
	endPnt = e->pos();
	this->isPressed = true;
}
void ui1::mouseMoveEvent(QMouseEvent *e) {
	if (this->isPressed) {
		endPnt = e->pos();

		myLine* line = new myLine;  //put the new line into vector  
		line->startPnt = startPnt;
		line->endPnt = endPnt;
		this->lines.push_back(line);

		update();                                    //repainter��call paintEvent  
		startPnt = endPnt;
	}
}
void ui1::mouseReleaseEvent(QMouseEvent *e) {
	setCursor(Qt::ArrowCursor);
	this->isPressed = false;
}
void ui1::reset() {
	lines.clear();
	generate();
}

QImage ui1::Mat2QImage(const Mat & mat)
{
	// 8-bits unsigned, NO. OF CHANNELS = 1
	if (mat.type() == CV_8UC1)
	{
		QImage image(mat.cols, mat.rows, QImage::Format_Indexed8);
		// Set the color table (used to translate colour indexes to qRgb values)
		image.setColorCount(256);
		for (int i = 0; i < 256; i++)
		{
			image.setColor(i, qRgb(i, i, i));
		}
		// Copy input Mat
		uchar *pSrc = mat.data;
		for (int row = 0; row < mat.rows; row++)
		{
			uchar *pDest = image.scanLine(row);
			memcpy(pDest, pSrc, mat.cols);
			pSrc += mat.step;
		}
		return image;
	}
	// 8-bits unsigned, NO. OF CHANNELS = 3
	else if (mat.type() == CV_8UC3)
	{
		// Copy input Mat
		const uchar *pSrc = (const uchar*)mat.data;
		// Create QImage with same dimensions as input Mat
		QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_RGB888);
		return image.rgbSwapped();
	}
	else if (mat.type() == CV_8UC4)
	{
		// Copy input Mat
		const uchar *pSrc = (const uchar*)mat.data;
		// Create QImage with same dimensions as input Mat
		QImage image(pSrc, mat.cols, mat.rows, mat.step, QImage::Format_ARGB32);
		return image.copy();
	}
	else
	{
		return QImage();
	}
}

float ui1::average(int x, int y)
{
	float l = 0.0f;
	for (int i = x * 10; i <= x * 10 + 9; i++)
	{
		for (int j = y * 10; j <= y * 10 + 9; j++)
		{
			Vec3b pixel = img.at<Vec3b>(i, j);
			float ave = 0.114*pixel[0] + 0.587*pixel[1] + 0.299*pixel[2];//Brightness = 0.299*R + 0.587*G + 0.114*B
			l += ave;
		}
	}
	l /= 100.0;
	return l;
}

void ui1::adjust1(int x, int y, float a)
{
	for (int i = x * 10 + 2; i <= x * 10 + 7; i++)
	{
		for (int j = y * 10 + 2; j <= y * 10 + 7; j++)
		{
			Vec3b pixel = img.at<Vec3b>(i, j);
			for (int k = 0; k <= 2; k++)
			{
				if (pixel[k] * a <= 255)
				{
					pixel[k] *= a;
				}
				else
				{
					pixel[k] = 255;
				}
			}
			img.at<Vec3b>(i, j) = pixel;
		}
	}
}

void ui1::adjust2(int x, int y, float a)
{
	for (int i = x * 10; i <= x * 10 + 9; i++)
	{
		for (int j = y * 10; j <= y * 10 + 9; j++)
		{
			Vec3b pixel = img.at<Vec3b>(i, j);
			for (int k = 0; k <= 2; k++)
			{
				if (pixel[k] * a <= 255)
				{
					pixel[k] *= a;
				}
				else
				{
					pixel[k] = 255;
				}
			}
			img.at<Vec3b>(i, j) = pixel;
		}
	}
}

