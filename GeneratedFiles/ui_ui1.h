/********************************************************************************
** Form generated from reading UI file 'ui1.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UI1_H
#define UI_UI1_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ui1Class
{
public:
    QWidget *centralWidget;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label2;
    QLabel *label;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ui1Class)
    {
        if (ui1Class->objectName().isEmpty())
            ui1Class->setObjectName(QStringLiteral("ui1Class"));
        ui1Class->resize(958, 568);
        centralWidget = new QWidget(ui1Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(70, 440, 221, 31));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(440, 440, 75, 23));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(600, 440, 75, 23));
        label2 = new QLabel(centralWidget);
        label2->setObjectName(QStringLiteral("label2"));
        label2->setGeometry(QRect(430, 10, 410, 410));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, 420, 71, 61));
        ui1Class->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(ui1Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 958, 23));
        ui1Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(ui1Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        ui1Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(ui1Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        ui1Class->setStatusBar(statusBar);

        retranslateUi(ui1Class);

        QMetaObject::connectSlotsByName(ui1Class);
    } // setupUi

    void retranslateUi(QMainWindow *ui1Class)
    {
        ui1Class->setWindowTitle(QApplication::translate("ui1Class", "ui1", 0));
        pushButton->setText(QApplication::translate("ui1Class", "Convert", 0));
        pushButton_2->setText(QApplication::translate("ui1Class", "Reset", 0));
        label2->setText(QString());
        label->setText(QApplication::translate("ui1Class", "Data", 0));
    } // retranslateUi

};

namespace Ui {
    class ui1Class: public Ui_ui1Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UI1_H
